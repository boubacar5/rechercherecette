const envoye = document.querySelector('.envoye');

envoye.addEventListener('click', rechercheRecette);

function rechercheRecette(){

    let inputTxt = document.getElementById('input').value.trim();

    fetch(`www.themealdb.com/api/json/v1/1/filter.php?a=${inputTxt}`)

    .then(response => response.json())
    .then(data => {

        let html = "";
        if (data.meals) {
            data.meals.forEach(meal => {
                html += `
                <div class="carte" >
                    ${meal.meals}
                </div>
                `
            })
        } else {
            html = `Recette non trouvable`
        }

        recette.innerHTML = html;
    })
}