const envoye = document.querySelector('.envoye');
const recette = document.querySelector('#recette');  

const popupBtn = document.querySelector('.popupBtn');
const popup = document.querySelector('.popup');
const fermer = document.querySelector('.fermer');

const apiKey = "b2354e484f5d3d136bd14873f1c07fc6";


envoye.addEventListener('click', rechercheRecette);
popup.addEventListener('click', popUp);


function rechercheRecette(){

    let inputTxt = document.getElementById('input').value.trim();


    fetch(`https://www.themealdb.com/api/json/v1/1/filter.php?i=${inputTxt}`)

    .then(response => response.json())
    .then(data => {

        let html = "";
        if (data.meals) {
            data.meals.forEach(meal => {
                html += `
                <div class="carte" data-id = "${meal.idMeal}">
                    <img src="${meal.strMealThumb}" class="recetteimg">
                    <h3 class="nomrecette" >${meal.strMeal}</h3>
                    <a href="#" class="description">Recette</a>
                </div>
                `
            })
        } else {
            html = `Recette non trouvable`
        }

        recette.innerHTML = html;
    })
}

function popUp(e){

    e.preventDefault();
    if(e.target.classList.contains('popup')) {
        let instructionRecette = e.target.parentElement.parentElement;
        fetch(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${instructionRecette.dataset.id}`)
        .then(response => response.json())
        .then(data => popUpRecette(data.meals))

    }

}

function popUpRecette(meal){
    console.log(meal)
    meal = meal[0];
    let html = `
    
        <h2 class="nomrecette">${meal.strMeal}</h2>
        <p>${meal.strInstructions}</p>
    <div>
        <a href="${meal.strYoutube}" target="_blank" >Regardez le video</a>
    </div>
    `
    popup.innerHTML = html;
    popup.parentElement.classList.add('affichage')

}
